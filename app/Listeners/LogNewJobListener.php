<?php

namespace App\Listeners;

use App\Events\JobCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogNewJobListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  JobCreatedEvent  $event
     * @return void
     */
    public function handle(JobCreatedEvent $event)
    {
        info('new.graphql.job', $event->job->toArray());
    }
}
