<?php

namespace App\Events;
use App\Job;

class JobCreatedEvent extends Event
{

	/**
     * Job
     * @var \App\Job
     */
    public $job;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Job $job)
    {
        $this->job = $job;
    }
}
