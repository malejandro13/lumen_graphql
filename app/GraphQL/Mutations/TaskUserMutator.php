<?php

namespace App\Graphql\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\TaskUser;
use App\Traits\GraphqlResponser;

class TaskUserMutator
{
    use GraphqlResponser;

    public function upsertUsersByTask($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        foreach ($args['users_id'] as $user_id) {
            TaskUser::updateOrCreate(
                ['task_id' => $args['task_id'],  'user_id' => $users_id]
            );
        }
        $taskUser = null;

        return $this->successResponse('taskUser', $taskUser, 'Registro creado satisfactoriamente', 200);

    }

    public function deleteUsersByTask($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $taskUser = null;

        return $this->successResponse('taskUser', $taskUser, 'Registro creado satisfactoriamente', 200);
    }

    public function restoreUsersByTask($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
         $taskUser = null;

        return $this->successResponse('taskUser', $taskUser, 'Registro creado satisfactoriamente', 200);
    }
}
